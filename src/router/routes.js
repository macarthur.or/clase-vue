import Home from '../pages/home.vue'
import Logout from '../pages/default.vue'

import Login from '../components/auth/Login.vue'
import Registro from '../components/auth/registro.vue'
import Recover from '../components/auth/Recover.vue'

export default [
  {
    path: '/app',
    name: 'App',
    component: Home,
    meta: { requireAuth: true }
  },
  {
    path: '/',
    component: Logout,
    children: [
      {
        path: '',
        name: 'login',
        component: Login,
        meta: { requireAuth: false }
      },
      {
        path: '/signin',
        name: 'Signin',
        component: Registro,
        meta: { requireAuth: false }
      },
      {
        path: '/recover',
        name: 'Recover',
        component: Recover,
        meta: { requireAuth: false }
      }
    ]
  }
]